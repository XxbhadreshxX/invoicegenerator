package com.bhadresh.invoicegenerator.activity;

import android.content.Context;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Adapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.bhadresh.invoicegenerator.R;
import android.app.DatePickerDialog;

import java.util.Calendar;

public class activity_invoiceform extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    item_list_adapter item_list_adapter;
    RecyclerView rcvitems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoiceform);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        EditText etdatee = findViewById(R.id.etdate);
        final Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                activity_invoiceform.this,(datePicker,year,month,day)  -> etdatee.setText(String.format("%02d",day) + "/" + String.format("%02d",month) + "/" + String.format("%02d",year)),newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        etdatee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    void bindViewValues(){
        item_list_adapter=new item_list_adapter(this);
        rcvitems.setAdapter(item_list_adapter);
    }

}